package testjpa.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import testjpa.modelo.Persona;

@Stateless
public class PersonaDAO {
	
	
	@Inject
	private EntityManager em;
	
	public void save(Persona p) {
		if(this.read(p.getCodigo())!=null)
			this.update(p);
		else
			this.create(p);
		
	}

	
	public void create(Persona p) {
		em.persist(p);
	}
	
	public Persona read(int id) {
		return em.find(Persona.class, id);
	}
	
	/**
	 * Opcion 1 de consulta por demanda para relacion LAZY
	 * */
	public Persona read2(int id) {
		Persona p = em.find(Persona.class, id);
		p.getTelefonos().size();
		
		return p;
	}
	
	public Persona read3(int id) {
		/*
		 * String jpql = "SELECT p "
					+ "	 FROM Persona p "
					+ "		  JOIN FETCH p.telefonos t "
					+ "		  JOIN FETCH p.direcciones d 	"
					+ " WHERE p.codigo = ?";*/
		
		String jpql = "SELECT p "
					+ "	 FROM Persona p "
					+ "		  JOIN FETCH p.telefonos t "
					+ " WHERE p.codigo = :a";
		
		Query q = em.createQuery(jpql, Persona.class);
		q.setParameter("a", id);
		Persona p = (Persona) q.getSingleResult();
		
		return p;
	}
	
	public void update(Persona p) {
		
		em.merge(p);
	}
	
	public void delete(int id) {
		Persona p = read(id);
		em.remove(p);
	}
	
	public List<Persona> getPersonas(){
		String jpql = "SELECT p FROM Persona p ";
		
		Query q = em.createQuery(jpql, Persona.class);
		
		List<Persona> personas = q.getResultList();
		return personas;
	}
	
	public List<Persona> getPersonas2(){
		String jpql = "SELECT p FROM Persona p ";
		
		Query q = em.createQuery(jpql, Persona.class);
		
		List<Persona> personas = q.getResultList();
		
		for(Persona p: personas) {
			p.getTelefonos().size();
		}
		
		return personas;
	}
	
	public List<Persona> getPersonasPorNombre(String filtroBusqued){
		String jpql = "SELECT p FROM Persona p "
					+ "	WHERE p.nombre LIKE :filtro ";
		
		Query q = em.createQuery(jpql, Persona.class);
		q.setParameter("filtro", "%"+filtroBusqued+"%");
		
		List<Persona> personas = q.getResultList();
		return personas;
	}
	
	
}
