package testjpa.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import testjpa.bussiness.GetionTelefonoON;
import testjpa.bussiness.Instalacion;
import testjpa.modelo.Persona;
import testjpa.modelo.Telefono;
import testjpa.modelo.TipoTelefono;

@ManagedBean
@ViewScoped
public class TelefonoController {

	private Telefono telefono;
	
	@Inject
	private Instalacion insON;
	
	
	@Inject
	private GetionTelefonoON telON; 
	
	@Inject
	private FacesContext fc;
	
	@PostConstruct
	public void init() {
		telefono = new Telefono();
	}

	public Telefono getTelefono() {
		return telefono;
	}

	public void setTelefono(Telefono telefono) {
		this.telefono = telefono;
	}
	
	public String guardarDatos() {
		telON.guardarTelefono(telefono);
		
		return "listado-personas";
	}
	
	public void consultarTipoTelefono(){
		
		List<TipoTelefono> tipos =  insON.getTelefonos();
		for(TipoTelefono tipo: tipos) {
			if(telefono.getIdTipo()==tipo.getCodigo())
				telefono.setTipo(tipo);
		}
		
	}
	
	public void consultarPersona() {
		
		Persona p;
		try {
			p = telON.consultarPersona(telefono.getIdPersonaTemp());
			telefono.setPersona(p);
		} catch (Exception e) {
			telefono.setPersona(null);
			// TODO Auto-generated catch block
			FacesMessage msg =  new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
			fc.addMessage("txtPersona", msg);
			
			e.printStackTrace();
		}
		
		
		
		
	}
}
