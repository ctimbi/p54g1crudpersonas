package testjpa.modelo;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Cuenta {

	@Id
	private int codigo;
	
	@OneToOne
	private Persona cliente;
	private Date fechaApertura;
	@OneToOne
	private TipoTelefono tipoCuenta;
	private double saldo;
	
	@OneToMany
	private List<Movimiento> movimientos;
	
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public Persona getCliente() {
		return cliente;
	}
	public void setCliente(Persona cliente) {
		this.cliente = cliente;
	}
	public Date getFechaApertura() {
		return fechaApertura;
	}
	public void setFechaApertura(Date fechaApertura) {
		this.fechaApertura = fechaApertura;
	}
	public TipoTelefono getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(TipoTelefono tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	
	
}
