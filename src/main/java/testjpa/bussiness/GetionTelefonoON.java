package testjpa.bussiness;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import testjpa.dao.PersonaDAO;
import testjpa.modelo.Persona;
import testjpa.modelo.Telefono;

@Stateless
public class GetionTelefonoON {

	@Inject
	private PersonaDAO daoPersona;
	
	@Inject
	private EntityManager em;
	
	public void guardarTelefono(Telefono telefono) {
		em.persist(telefono);
	}
	
	
	public Persona consultarPersona(int codigoPersona) throws Exception {
		
		
		Persona p = daoPersona.read(codigoPersona);
		if(p==null)
			throw new Exception("Persona no existe");
		
		return p;
	}
	
	
}
