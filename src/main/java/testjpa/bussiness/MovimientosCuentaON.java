package testjpa.bussiness;

import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;

import testjpa.dao.CuentaDAO;
import testjpa.dao.MovimientoDAO;
import testjpa.dao.PersonaDAO;
import testjpa.modelo.Cuenta;
import testjpa.modelo.Movimiento;

@Stateless
public class MovimientosCuentaON {

	@Inject
	private CuentaDAO daoCuenca;
	
	@Inject
	private MovimientoDAO daoMov;
	
	@Inject
	private PersonaDAO daoPer;
	
	public void deposito(int codigoCuenta, String cedula, double valorDeposito) {
		
		Cuenta c = daoCuenca.getCuenta(codigoCuenta, cedula);
		if(c==null)
			throws new Exception("Cuenta no existe");
		
		c.setSaldo(c.getSaldo() + valorDeposito);
		
		Movimiento m = new Movimiento();
		m.setDescripcion("Deposito desde terceros");
		m.setFechaTransaccion(new Date());
		m.setValor(valorDeposito);
		m.setTipoTransaccion(5);   //Deposito
		
		c.addMovimiento(m);
		
		daoCuenta.update(c);
		
		
		
	}
	
	
	public void retiro(int codigoCuenta, String cedula, double valorRetiro) throws Exception {
		
		Cuenta c = daoCuenca.getCuenta(codigoCuenta, cedula);
		if(c==null)
			throws new Exception("Cuenta no existe");
		
		if(c.getSaldo()< valorRetiro)
			throws new Exception("Saldo insuficiente");
		
		c.setSaldo(c.getSaldo() - valorRetiro);
		
		Movimiento m = new Movimiento();
		m.setDescripcion("Retiro desde terceros");
		m.setFechaTransaccion(new Date());
		m.setValor(valorRetiro);
		m.setTipoTransaccion(7);   //Deposito
		
		c.addMovimiento(m);
		
		daoCuenta.update(c);
		
		
		
	}
	
	
	
}
