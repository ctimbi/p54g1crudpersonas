package testjpa.bussiness;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;

import testjpa.dao.PersonaDAO;
import testjpa.modelo.Persona;
import testjpa.modelo.Telefono;
import testjpa.modelo.TipoTelefono;

@Stateless
public class PersonaON {
	
	@Inject
	private Instalacion init;

	@Inject
	private PersonaDAO dao;
	
	public void guardar(Persona p) throws Exception{
		
		if(p.getNombre().length()<5)
			throw new Exception("Dimension corta");
		
		
		List<TipoTelefono> tipos = init.getTelefonos();
		
		for(Telefono tel : p.getTelefonos()) {
			for(TipoTelefono tt : tipos) {
				if(tel.getIdTipo() == tt.getCodigo())
					tel.setTipo(tt);
			}
		}
		
		dao.save(p);		
	}
	
	public List<Persona> getListadoPersonas(){
		return dao.getPersonas2();
	}
	
	public void borrar(int codigo) throws Exception {
		try {
			dao.delete(codigo);
		}catch(Exception e) {
			throw new Exception("Error al borrar " + e.getMessage());
		}
		
	}
	
	public Persona getPersona(int codigo) {
		Persona aux = dao.read3(codigo);
		
		return aux;
		
	}
	
	
}
