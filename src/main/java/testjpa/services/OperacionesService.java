package testjpa.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/operaciones")
public class OperacionesService {

	@GET
	@Path("/suma")
	@Produces("application/json")
	public int sumar(@QueryParam("xx") int a, @QueryParam("yy") int b) {
		return a + b;
	}
	
	@GET
	@Path("/suma/{a}/{b}")
	@Produces("application/json")
	public int sumar2(@PathParam("a") int a, @PathParam("b") int b) {
		return a + b;
	}
	
	
	@POST
	@Path("/accion")
	@Produces("application/json")
	@Consumes("application/json")
	public Respuesta accionX(Objeto obj) {
		
		System.out.println("p1" + obj.getParam1() + "  p2= " + obj.getParam2());
		
		Respuesta r = new Respuesta();
		r.setCodigo(99);
		r.setMensaje("OK");
		
		return r;
	}
	
	
	
	@GET
	@Path("/accion")
	@Produces("application/json")
	
	public Respuesta accionX() {
		
		
		Respuesta r = new Respuesta();
		r.setCodigo(1);
		r.setMensaje("OKOK");
		
		return r;
	}
}
