package testjpa.services.modelo;

public class ListaPersonasNombre {

	private String nombre;
	
	public ListaPersonasNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
