package testjpa.services;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import testjpa.bussiness.MovimientosCuentaON;
import testjpa.modelo.Cuenta;
import testjpa.modelo.Movimiento;
import testjpa.modelo.Persona;
import testjpa.services.modelo.TrxDeposito;

@Path("cuenta")
public class CuentaService {

	private EntityManager em;
	
	private MovimientosCuentaON gMov;
	
	public Respuesta deposito(Cuenta c, Movimiento m) {

		/*sola */
		em.persist(c);
		em.persist(m);
		
		return null;
	}
	
	public Respuesta deposito2(Movimiento m) {

		em.persist(m);
		
		return null;
	}
	
	public Respuesta deposito3(Cuenta c) {

		em.persist(c);
		return null;
	}
	
	@POST
	@Path("/deposito")
	@Produces("application/json")
	@Consumes("application/json")
	public Respuesta deposito3(TrxDeposito trx) {

		try {
			gMov.deposito(trx.getCodigoCuenta(), trx.getCedula(), trx.getValorDeposito());
		}catch(Exception e) {
			Respuesta r = new Respuesta();
			r.setCodigo(99);
			r.setMensaje(e.getMessage());
		}
		return null;
	}
	
	
	
	
}
