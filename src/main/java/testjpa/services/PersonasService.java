package testjpa.services;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import testjpa.bussiness.PersonaON;
import testjpa.modelo.Persona;
import testjpa.services.modelo.ListaPersonasNombre;

@Path("/personas")
public class PersonasService {

	@Inject
	private PersonaON pON;
	
	@GET
	@Path("listadocon")
	@Produces("application/json")
	public List<Persona> getPersonas(){
		
		List<Persona> listado = pON.getListadoPersonas();
		
		return listado;
	}
	
	
	@GET
	@Path("listadosolo")
	@Produces("application/json")
	public List<ListaPersonasNombre> getPersonasSoloNombres(){
		List<ListaPersonasNombre> nombres = new ArrayList<>();
		
		List<Persona> listado = pON.getListadoPersonas();
		for(Persona p : listado) {
			
			nombres.add(new ListaPersonasNombre(p.getNombre()));
		}
		
		return nombres;
	}
	
	@GET
	@Path("listadosin")
	@Produces("application/json")
	public List<Persona> getPersonasSinTelefonos(){
		
		List<Persona> listado = pON.getListadoPersonas();
		for(Persona p : listado) {
			p.setTelefonos(null);
		}
		
		return listado;
	}
}
